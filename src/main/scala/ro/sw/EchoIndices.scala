package ro.sw

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

import scala.collection.JavaConversions.`deprecated asScalaIterator`
import scala.collection.JavaConversions.`deprecated iterableAsScalaIterable`
import scala.collection.Searching.Found
import scala.collection.Searching.InsertionPoint
import scala.collection.Searching.SearchResult
import scala.collection.Searching.search
import scala.collection.mutable.Map
import scala.io.Source
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import scala.xml.XML

import org.apache.commons.lang3.StringEscapeUtils

import net.sf.saxon.Configuration
import net.sf.saxon.s9api.Processor
import net.sf.saxon.s9api.XdmNode
import opennlp.tools.tokenize.SimpleTokenizer
import opennlp.tools.stemmer.snowball.SnowballStemmer
import javax.xml.transform.stream.StreamSource
import java.io.StringReader

import com.opencsv.CSVWriter
import scala.collection.mutable.ListBuffer

object EchoIndices extends App {

  val digitsRegex = """\d+""".r
  val commaRegex = """\s*,\s*""".r

  val articlesInDir = "../mirela/articole-in"
  val articlesOutDir = "../OUT-articles"
  val indicesDir = "../mirela/fisiere-txt-client"
  val outDir = "../OUT-indices"
  val ambiguousIndicesOutFileName = s"${outDir}/ambiguous-indices.csv"
  val parRegex = """(?s)<p\b.*?>(.+?)<\/p>""".r
  val commentsWithPageRegex = """<!--(<page [^>]+/>)-->""".r
  val indexPageRegex = """<page\s+n="(\d+)"""".r
  val sectionRegex = """<b>(.)<\/b>""".r
  val headRegex = """<head>(.+)<\/head>""".r
  val indexRegex = """(?is)(.+?)(?:\s+((?:(?:<i>)?\d+(?:\-\d+)?(?:</i>)?|[ivxlcmd]+)(?:,\s*(?:\d+(?:\-\d+)?|[ivxlcmd]+))*))?""".r

  //  val articleXpath = "//articleentry"
  //  val titleXpath = "mainentry"
  //  val pageXpath = "toc-p/@page"

  val articleXpath = "//complexarticle"
  val titleXpath = "@entry"
  val startPageXpath = "@page"
  val pageXpath = "//page"
  val idXpath = "@id"
  
  val romanChars = Seq('I', 'V', 'X', 'L', 'C', 'M', 'D')

  enrichArticles(articlesInDir, articlesOutDir)
  val articles = parseArticles(articlesOutDir)
  val (articlesWithPages, articlesNotParsed) = articles.partition(p => p.isSuccess)
  //  filter and report wrong articles
  articlesNotParsed.foreach {
    article =>
      article.recover {
        case e =>
          println(e.getMessage)
      }
  }
  // sort articles by page no and id
  val parsedArticles = articlesWithPages.map(_.get).toList
//    .sortWith {
//      (a1, a2) => a1.startPage.no < a2.startPage.no || a1.startPage == a2.startPage && a1.id < a2.id
//    }
//  parsedArticles foreach println
//  val articlesByPage = parsedArticles.groupBy(_.startPage.no)
//  val articlePagesSorted = articlesByPage.keySet.toList.sorted

  val indices: List[Try[ParsedIndex]] = parseIndices(indicesDir)
  val (validIndices, wrongIndices) = indices.partition(i => i.isSuccess)
  //  filter and report wrong indices
  wrongIndices.foreach {
    article =>
      article.recover {
        case e =>
          println(e.getMessage)
      }
  }
  val parsedIndices = validIndices.map(_.get).groupBy(_.section)
  //  validIndices foreach println
  val tokenizer = SimpleTokenizer.INSTANCE
  val stemmer = new SnowballStemmer(SnowballStemmer.ALGORITHM.ENGLISH)
  var newIndexId = 0
  val newIndices = validIndices.map(_.get).map {
    parsedIndex =>
      val articles = parsedIndex.pages.map {
        page =>
          val foundArticles = parsedArticles.filter(a => a.sections.exists(s => s.sectionPage.page == page))
          if (foundArticles.length == 1)
            Left(foundArticles.head)
          else {
            val articlesMatchTerm = foundArticles.filter {
              art =>
                art.sections
                .filter(section => section.sectionPage.page == page)
                .exists(section => {
                  val terms = parsedIndex.terms
                    .replaceAll("<[^>]+?>", "")
                    .replaceAll("\\(s\\)", "")
                    .replaceAll(",", "")
                    .toLowerCase
                    
                  val sectionContent = section.content(art)
                  val sectionWords = tokenizer.tokenize(sectionContent).toList
                  val termList = tokenizer.tokenize(terms).toList
                  
//                    sectionWords
//                      .map(_.toLowerCase)
//                      .map(stemmer.stem)
//                      .containsSlice(
//                        termList
//                          .map(_.toLowerCase)
//                          .map(stemmer.stem)
//                      )
                  
                  termList
                    .map(_.toLowerCase)
                    .map(stemmer.stem)
                    .forall(
                      sectionWords
                        .map(_.toLowerCase)
                        .map(stemmer.stem)
                        .contains)
                })
            }
            if (articlesMatchTerm.length == 1)
                Left(articlesMatchTerm.head)
            else if (articlesMatchTerm.length > 1) {
//                println(s"""Term(s) "${parsedIndex.terms}" found in $page: ${
//                  foundArticles.map(_.file.getName).mkString(", ")
//                }""")
                Right(FoundInArticles(articlesMatchTerm))
            } else {
//                println(s"""Term(s) "${parsedIndex.terms}" not found in $page: ${
//                  foundArticles.map(_.file.getName).mkString(", ")
//                }""")
                Right(NotFoundInArticles(foundArticles))
            }
          }
      }
      val newIndex = NewIndex(newIndexId, parsedIndex, articles)
      newIndexId+=1
      newIndex
  }

  // newIndices foreach println
  val ambiguousIndicesOutFile = new BufferedWriter(new FileWriter(ambiguousIndicesOutFileName))
  val csvWriter = new CSVWriter(ambiguousIndicesOutFile)
  val csvFields = Array("Index term", "Index page", "Art. title", "Art. file")
  csvWriter.writeNext(csvFields)

  val newIndicesBySection = newIndices.groupBy(_.parsedIndex.section)
  for (newIndicesOfSection <- newIndicesBySection) {
    val letter = newIndicesOfSection._1
    val indexArticle = newIndexFileName(letter)
    val fileName = outDir + "/2590-3004_ECHO_fulltextxml_" + indexArticle + ".xml"
    val bw = new BufferedWriter(new FileWriter(fileName))
    bw.write(beginFile(indexArticle, newIndicesOfSection._2.head.parsedIndex.page.map(_.no.toString).getOrElse("none"), letter))
    for (newIndex <- newIndicesOfSection._2) {
      bw.write(indexPattern(newIndex, newIndices))
    }
    bw.write(endFile)
    bw.close()
  }
  csvWriter.close()
  ambiguousIndicesOutFile.close()
  
  def newIndexFileName(letter: String ) = "SIM-INDEX-" + letter 
  def newIndexId(id: Int) = "i" + id
  
  def beginFile(indexArticleId: String, indexArticlePage: String, letter: String) = s"""
    |<?xml version="1.0" encoding="UTF-8"?>
    |<!DOCTYPE encyclopedia PUBLIC "-//Semantico//XML DTD Brill EQ v1.1//EN" "Encyclopedia.dtd">
    |<encyclopedia>
    |<div2>
    |<head></head>
    |<art>
    |<complexarticle entry="$letter" id="${indexArticleId}" page="${indexArticlePage}">
    |<pseudoarticle>
    |<articleentry>
    |<mainentry>${letter}</mainentry>
    |</articleentry>
    |<p>
    |<list type="simple">
    """.stripMargin
    
  def endFile = s"""
    |</list>
    |</p>
    |</pseudoarticle>
    |</complexarticle>  
    |</art>
    |</div2>
    |</encyclopedia>
  """.stripMargin

  def indexPattern(newIndex: NewIndex, newIndices: List[NewIndex]): String = {
    val articlesByTitle = newIndex.articles.groupBy(identity)
    val mutipleReferences: Map[String, Int] = Map()
    val seeAlsoLink: List[String] = newIndex.parsedIndex.seeAlso.map {
      targetIndexTerms =>
        val targetIndexOpt = newIndices.find(ni => ni.parsedIndex.terms == targetIndexTerms)
        targetIndexOpt match {
          case Some(targetIndex) =>
            s"""<link n="${newIndexId(targetIndex.id)}" targets="${newIndexFileName(targetIndex.parsedIndex.section)}" type="echo">$targetIndexTerms</link>"""
          case None =>
            s"<!-- Index not found for: ${targetIndexTerms} -->"
        }
    }.toList
    val links = (seeAlsoLink ++ {
      newIndex.parsedIndex.pages.zip(newIndex.articles) map {
        articlesPerPage: (Page, Either[Article, Articles]) =>
            articlesPerPage._2 match {
              case Left(article) =>
                List(s"""<link n="${
                  val firstSectionForPage = article.sections.filter(section => section.sectionPage.page == articlesPerPage._1).head
                  anchorId(firstSectionForPage.volume.str, firstSectionForPage.sectionPage.str, firstSectionForPage.column.str)
                }" targets="${
                  article.articleId
                }" type="echo">${
                  articlesByTitle.get(Left(article)) match {
                    case Some(listOfArticles) =>
                      if (listOfArticles.size == 1)
                        article.title
                      else {
                        if (mutipleReferences.contains(article.title)) {
                          mutipleReferences.put(article.title, mutipleReferences.get(article.title).get + 1)
                        } else {
                          mutipleReferences.put(article.title, 1)
                        }
                        article.title + " (" + mutipleReferences.get(article.title).get + ")"
                      }
                    case None =>
                      article.title
                  }
                }</link>""")
              case Right(articles) =>
                articles match {
                  case foundArticles: FoundInArticles =>
                    foundArticles.list.map {
                      article =>
                        s"""<link n="${
                          val firstSectionForPage = article.sections.filter(section => section.sectionPage.page == articlesPerPage._1).head
                          anchorId(firstSectionForPage.volume.str, firstSectionForPage.sectionPage.str, firstSectionForPage.column.str)
                        }" targets="${
                          article.articleId
                        }" type="echo">${
                          articlesByTitle.get(Left(article)) match {
                            case Some(listOfArticles) =>
                              if (listOfArticles.size == 1)
                                article.title
                              else {
                                if (mutipleReferences.contains(article.title)) {
                                  mutipleReferences.put(article.title, mutipleReferences.get(article.title).get + 1)
                                } else {
                                  mutipleReferences.put(article.title, 1)
                                }
                                article.title + " (" + mutipleReferences.get(article.title).get + ")"
                              }
                            case None =>
                              article.title
                          }
                        }</link>"""
                    }
                  case notFoundInArticles: NotFoundInArticles =>
                    // write ambiguous articles
                    csvWriter.writeNext(Array(newIndex.parsedIndex.terms, articlesPerPage._1.no.toString(), articles.list.head.title, articles.list.head.file.getName))
                    articles.list.tail.foreach {
                      art =>
                        csvWriter.writeNext(Array("", "", art.title, art.file.getName))
                    }
                    Nil
                  case _ =>
                    Nil
                }
            }
      }
    }.flatten)
    
    if (links.length > 0)
      s"""
      |  <label>
      |    <p>
      |      <anchor id="${newIndexId(newIndex.id)}"/>
      |      ${transformTerms(newIndex.parsedIndex.terms)}
      |    </p>
      |  </label>
      |  <item>
      |    <p>${links.mkString(" | ")}</p>
      |  </item>
      """.stripMargin
    else ""
  }
    
  def transformTerms(terms: String): String =
    terms
      .replaceAll("<i>", "<hi rend=\"italic\">")
      .replaceAll("<b>", "<hi rend=\"bold\">")
      .replaceAll("<sup>", "<hi rend=\"sup\">")
      .replaceAll("<sub>", "<hi rend=\"sub\">")
      .replaceAll("</(i|b|sup|sub)>", "</hi>")
      
  def anchorId(v: String, p: String, c: String) = s"echoorg_P-V${v}P${p}C${c}"
      
  def enrichArticles(articlesInDir: String, articlesOutDir: String): List[String] = {
    // <!--<page id="P-V1P4C1" c="1" p="4a" v="1"/>-->
    val cRegex = """c="([^"]+)"""".r
    val pRegex = """p="([^"]+)"""".r
    val vRegex = """v="([^"]+)"""".r
//    val idRegex = """id="([^"]+)"""".r
    val nextArticleRegex = """^[\s\r\n]*<p>Next article:""".r
    
    val articleFiles = getListOfFiles(articlesInDir)
    val articles = (for (file <- articleFiles) yield {
      (Source.fromFile(file, "UTF-8").mkString, file)
    })
//    .map {
//      articleWithFile =>
//        val article = articleWithFile._1
//        val file = articleWithFile._2
//        
//        // replace final letters from pages
//        val pageMatches = commentsWithPageRegex.findAllMatchIn(article)
//        val newArticle = pageMatches.toList.sortBy(_.start).reverse.foldLeft(article) {
//          (art, pageMatch) =>
//            val cMatchOpt = cRegex.findFirstMatchIn(pageMatch.matched)
//            val pMatchOpt = pRegex.findFirstMatchIn(pageMatch.matched)
//            val idMatchOpt = idRegex.findFirstMatchIn(pageMatch.matched)
//            
//            if (cMatchOpt.isEmpty || pMatchOpt.isEmpty || idMatchOpt.isEmpty) {
//              println(s"Could not solve: id: ${idMatchOpt.map(_.matched)}; p: ${pMatchOpt.map(_.matched)}; c: ${cMatchOpt.map(_.matched)}")
//            }
//            
//            val newIdCol = for {
//              pMatch <- pMatchOpt
//              cMatch <- cMatchOpt
//              idMatch <- idMatchOpt
//            } yield {
//              val newCol = pMatch.group(1).last match {
//                case 'a' => "2"
//                case 'b' => "3"
//                case 'c' => "4"
//                case 'd' => "5"
//                case 'e' => "6"
//                case _ => cMatch.group(1)
//              }
//              (idMatch.group(1).replaceFirst("C\\d+", s"C${newCol}"), newCol)
//            }
//            
//            val newPage = 
//              if (idMatchOpt.map(_.group(1)) != newIdCol.map(_._1)) {
////                println(s"ID changed: ${(idMatchOpt.map(_.group(1)), pMatchOpt, cMatchOpt)} -> ${newIdCol.map(_._1)}")
//                pageMatch.group(1)
//                .replaceFirst("id=\"[^\"]+\"", "id=\"" + newIdCol.get._1 +"\"")
//                .replaceFirst("c=\"[^\"]+\"", "c=\"" + newIdCol.get._2 +"\"")
//              } else {
//                pageMatch.group(1)
//              }
//            art.patch(pageMatch.start(1), newPage, pageMatch.end(1) - pageMatch.start(1))
//        }
//        (newArticle, file)
//    }
    .map {
      articleWithFile =>
        val article = articleWithFile._1
        val file = articleWithFile._2
        
        // generate anchors
        val pageMatches = commentsWithPageRegex.findAllMatchIn(article)
        val newArticle = pageMatches.toList.sortBy(_.start).reverse.foldLeft(article) {
          (art, pageMatch) =>
            val vOpt = vRegex.findFirstMatchIn(pageMatch.matched).map(_.group(1))
            val pOpt = pRegex.findFirstMatchIn(pageMatch.matched).map(_.group(1))
            val cOpt = cRegex.findFirstMatchIn(pageMatch.matched).map(_.group(1))
            val anchor = s"""<anchor id="${anchorId(vOpt.get, pOpt.get, cOpt.get)}"/>"""
            if (nextArticleRegex.findFirstIn(art.substring(pageMatch.end +1)).isDefined)
              art // follows <p>Next article...
            else
              art.patch(pageMatch.end, anchor, 0)
        }

        (newArticle, file)
    }.map {
      articleWithFile =>
        val article = articleWithFile._1
        val file = articleWithFile._2

        val bw = new BufferedWriter(new FileWriter(articlesOutDir + File.separator + file.getName))
        bw.write(article)
        bw.close

        article
    }
    articles
  }
        

  /**
   * Parse articles given the folder containing the xml files.
   */
  def parseArticles(articlesDir: String) = {
    val configuration = new Configuration()
    val processor = new Processor(configuration)
    val builder = processor.newDocumentBuilder()
    builder.setDTDValidation(false)

    val xpathCompiler = processor.newXPathCompiler()
    val articleSelector = xpathCompiler.compile(articleXpath).load()
    val titleSelector = xpathCompiler.compile(titleXpath).load()
    val idSelector = xpathCompiler.compile(idXpath).load()
    val startPageSelector = xpathCompiler.compile(startPageXpath).load()
    val pageSelector = xpathCompiler.compile(pageXpath).load()

    val articleFiles = getListOfFiles(articlesDir)

    var id = 0
    val articles = (for (file <- articleFiles) yield {
      val articleContent = 
        Source.fromFile(file, "UTF-8").mkString
          .replaceAll("<!--(<page[^>]+>)-->", "$1")
      
      val xml = builder.build(new StreamSource(new StringReader(articleContent)))
      articleSelector.setContextItem(xml)

      for (article <- articleSelector.iterator().toList) yield {
        titleSelector.setContextItem(article)
        idSelector.setContextItem(article)
        startPageSelector.setContextItem(article)
        pageSelector.setContextItem(article)
        val title = titleSelector.evaluateSingle().getStringValue
        val articleId = idSelector.evaluateSingle().getStringValue
        val startPageValue = startPageSelector.evaluateSingle().getStringValue.dropWhile(c => c != ':').drop(1)
        val startPage = Page(startPageValue)
        
        val sections = pageSelector.map {
          pageXdmItem =>
            val pageXdmNode = pageXdmItem.asInstanceOf[XdmNode]
            val volStr = pageXdmNode.attribute("v")
            val pageStr = pageXdmNode.attribute("p")
            val colStr = pageXdmNode.attribute("c")
//            val idStr = pageXdmNode.attribute("id")
            Section(volStr, pageStr, colStr)
              .recoverWith {
                case e: Exception =>
                  Failure(new Exception(e.getMessage + s" For article ${title}, file ${file.getName}"))
              }
        }
        
//        sections foreach println
        
        val (validSections, errorSections) = sections.partition(p => p.isSuccess)
        
        if (errorSections.isEmpty) {
          startPage.map {
            page =>
              val article = Article(id, articleId, page, title, file, validSections.toSeq.map(s => s.get))
              id += 1
              article
          }.recoverWith {
            case e: Exception =>
              Failure(new Exception(e.getMessage + s" For article ${title}, file ${file.getName}"))
          }
        } else {
          Failure(new SectionException(errorSections.toSeq.map(f => f.asInstanceOf[Failure[Section]])))
        }
      }
    }).flatten
    articles
  }

  /**
   * Parse indices from xml files given the indices folder.
   */
  def parseIndices(indicesDir: String): List[Try[ParsedIndex]] = {
    var section = ""
    val indicesFiles = getListOfFiles(indicesDir, ".txt")
    val indices: List[Try[ParsedIndex]] = (for (file <- indicesFiles) yield {
      // println(file + ":")
      val fileString = StringEscapeUtils.unescapeHtml4(Source.fromFile(file, "UTF-16LE").mkString)
        .replaceAll("&Atod;", "\u0100")
        .replaceAll("&umcar;", "\u016B")
        .replaceAll("<head>", "<p><head>")
        .replaceAll("</head>", "</head></p>")

      val page = indexPageRegex.findFirstMatchIn(fileString).map(m => ArabPage(m.group(1).toInt))
      val indexPar = for (m <- parRegex.findAllMatchIn(fileString).toList) yield {
        val par = m.group(1).replaceAll("<col\\d+>", "")
        par match {
          case sectionRegex(s) =>
            section = s
            Success(None)
          case headRegex(s) =>
            section = "qpass"
            Success(None)
          case indexRegex(terms, pages) =>
            val seeAlsoMatch = """see( also)? """.r.findFirstMatchIn(terms)
            val index = seeAlsoMatch.map {
              m =>
                ParsedIndex(section, page, terms.substring(0, m.start).trim, indexPages(pages), Some(terms.substring(m.end)))
            }.getOrElse {
              ParsedIndex(section, page, terms, indexPages(pages))
            }
            if (index.pages.isEmpty && index.seeAlso.isEmpty)
              Failure(new Exception(s"Par not parsed: $par"))
            else
              Success(Some(index))
          case _ =>
            Failure(new Exception(s"Par not parsed: $par"))
        }
      }
      //    indexPar foreach println
      indexPar
        .filterNot(p => p.isSuccess && p.get.isEmpty) // filter out the sections which are None
        .map { // transform Success(Some(p)) into Success(p)
          case Success(r) =>
            Success(r.get)
          case Failure(e) =>
            Failure(e)
        }
    }).flatten
    indices
  }

  /**
   * Transform/ split a list of numbers separated by comma "," into a list of ints
   */
  def indexPages(pagesStr: String): List[Page] = {
    if (pagesStr == null) return Nil
    commaRegex
      .split(pagesStr.replaceAll("<[^>]+>", "")).toList
      .map {
      s => 
        val i = s.indexOf("-")
        if (i == -1)
          Page(s).toOption.toList
        else {
          val start = s.substring(0, i).toInt
          val end = s.substring(i + 1).toInt
          (start to end).toList.map(p => ArabPage(p))
        }
      }.flatten
  }

  def getListOfFiles(dir: String, extension: String = ".xml"): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles
        .filter(f => f.isFile && f.getName.toLowerCase().endsWith(extension))
        .sortWith((f1, f2) => f1.getName.compareTo(f2.getName) < 0)
        .toList
    } else {
      Nil
    }
  }

  def fromRoman( r:String ) : Int = {
    val arabicNumerals = List("CM"->900,"M"->1000,"CD"->400,"D"->500,"XC"->90,"C"->100,
                              "XL"->40,"L"->50,"IX"->9,"X"->10,"IV"->4,"V"->5,"I"->1)	
   
    var s = r                          
    arabicNumerals.foldLeft(0){ (n,t) => { 
      val l = s.length; s = s.replaceAll(t._1,""); val c = (l - s.length)/t._1.length  // Get the frequency
      n + (c*t._2)  // Add the arabic numerals up  
    } }
  }

}

abstract class Page(val no: Int)
object Page {
  def apply(pageValue: String): Try[Page] = {
    val pageEndingLetters = Seq('a', 'b', 'c', 'd', 'e')
    if (pageEndingLetters.contains(pageValue.charAt(pageValue.length - 1)) 
        && pageValue.dropRight(1).forall(_.isDigit)) {
      Success(ArabPage(pageValue.dropRight(1).toInt))
    } else if (pageValue.forall(_.isDigit)) {
      Success(ArabPage(pageValue.toInt))
    } else if (pageValue.toUpperCase.forall(c => EchoIndices.romanChars.contains(c))) {
      Success(RomanPage(EchoIndices.fromRoman(pageValue.toUpperCase())))
    } else {
      println(s"Unrecognized page value : $pageValue")
      Failure(new PageException(pageValue))
    }
  }
}
case class RomanPage(override val no: Int) extends Page(no)
case class ArabPage(override val no: Int) extends Page(no)

case class Volume(no: Int, str: String)
object Volume {
  def apply(str: String): Volume = Volume(str.toInt, str)
}
case class SectionPage(page: Page, str: String)
object SectionPage {
  def apply(str: String): Try[SectionPage] = Page(str).map(SectionPage(_, str))
}
case class Column(no: Int, str: String)
object Column {
  def apply(str: String): Column = Column(str.toInt, str)
}
case class Section(volume: Volume, sectionPage: SectionPage, column: Column) {
  self =>
  import Section._
  
  val articlePageRegex = """<page\s+[^>]+>""".r
  
  var _content = ""

  def content(article: Article): String = {
    if (_content.length() == 0)
      _content = grabContent(article)
    _content
  }

  def grabContent(article: ro.sw.Article): String = {
    val artContent = Source.fromFile(article.file, "UTF-8").mkString
      .replaceAll("<p>Next article: <xref.+?</p>", "")
   
    val pageMatchesWithIndices = {
      val extracted = articlePageRegex.findAllMatchIn(artContent)
      extracted.zipWithIndex.dropWhile {
          pageMatched =>
            val pageXml = XML.loadString(pageMatched._1.matched)
            val section = Section(pageXml \@ "v", pageXml \@ "p", pageXml \@ "c")
              .recoverWith {
                case e: Exception =>
                  Failure(new Exception(e.getMessage + s" For article ${article.title}, file ${article.file.getName}"))
              }
            
            !(section.isSuccess && self == section.get)
        }.take(2).toSeq
    }
    
    val startOffset = if (pageMatchesWithIndices(0)._2 == 0) 0 else pageMatchesWithIndices(0)._1.start
    if (pageMatchesWithIndices.size == 1) {
      artContent.substring(startOffset)
    } else {
      artContent.substring(startOffset, pageMatchesWithIndices(1)._1.start)
    }
      .replaceAll("<[^>]+?>", "")
  }
}
object Section {
  def apply(volStr: String, pageStr: String, colStr: String): Try[Section] = {
    SectionPage(pageStr).map(Section(Volume(volStr), _, Column(colStr)))
  }
}
case class Article(id: Int, articleId: String, startPage: Page, title: String, file: File, sections: Seq[Section])
trait Articles {
  def list: List[Article]
}
case class FoundInArticles(list: List[Article]) extends Articles
case class NotFoundInArticles(list: List[Article]) extends Articles

case class ParsedIndex(section: String, page: Option[Page], terms: String, pages: List[Page], seeAlso: Option[String] = None)
case class NewIndex(id: Int, parsedIndex: ParsedIndex, articles: List[Either[Article, Articles]])

final case class PageException(val pageString: String)
  extends Exception(s"Page '${pageString}' not parsed as a number.")
final case class SectionException(val sections: Seq[Failure[Section]])
  extends Exception(s"Sections not parsed: ${sections.map(_.exception.getMessage).mkString("\n")}")
